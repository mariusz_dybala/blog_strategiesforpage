using System;
using System.Windows.Input;
using FreshMvvm;
using StrategiesForPage.Interfaces;
using StrategiesForPage.Models;

namespace StrategiesForPage.Services
{
    public class EditPageService : IAddOrEditPageService
    {
        public string ButtonText => "Edit";
        public string HeaderText => "Edit your list";

        public ShoppingList ShoppingListModel { get; }

        public EditPageService(ShoppingList shoppingList)
        {
            ShoppingListModel = shoppingList;
        }

        public void OnButtonClicked(ShoppingList shoppingList, IPageModelCoreMethods coreMethods)
        {
            if (coreMethods == null)
            {
                throw new NullReferenceException("Core methods need to be set. Execute Init() method first.");
            }
            
            var newShoppingList = new ShoppingList
            {
                Description = shoppingList.Description,
                IsOpened = true,
                Id = shoppingList.Id,
                Owner = shoppingList.Owner,
                Name = shoppingList.Name,
                Type = shoppingList.Type
            };

            coreMethods.PopPageModel(PageMessage.AsEdit(newShoppingList), true, true);
        }
    }
}