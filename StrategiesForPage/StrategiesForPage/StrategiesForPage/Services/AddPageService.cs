using System;
using FreshMvvm;
using StrategiesForPage.Interfaces;
using StrategiesForPage.Models;

namespace StrategiesForPage.Services
{
    public class AddPageService : IAddOrEditPageService
    {
        public string ButtonText => "Add";

        public string HeaderText => "Add your new list";
        
        public ShoppingList ShoppingListModel { get; }

        public AddPageService()
        {
            ShoppingListModel = new ShoppingList();
        }

        public void OnButtonClicked(ShoppingList shoppingList, IPageModelCoreMethods coreMethods)
        {
            if (coreMethods == null)
            {
                throw new NullReferenceException("Core methods need to be set. Execute Init() method first.");
            }
            
            var itemsCount = new Random().Next(1, 50);
            var itemId = Guid.NewGuid();

            var newShoppingList = new ShoppingList
            {
                Description = shoppingList.Description,
                IsOpened = true,
                ItemsCount = itemsCount,
                Id = itemId.ToString(),
                Owner = shoppingList.Owner,
                Name = shoppingList.Description,
                Type = shoppingList.Type
            };

            coreMethods.PopPageModel(PageMessage.AsAdd(newShoppingList), true, true);
        }
    }
}