using System.Windows.Input;

namespace StrategiesForPage.Models
{
    public class ShoppingList
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Owner { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public bool IsOpened { get; set; }
        
        public int ItemsCount { get; set; }
        
        public ICommand DoneClickCommand { get; set; }
        public ICommand EditClickCommand { get; set; }

        public static ShoppingList Copy(ShoppingList shoppingList)
        {
            var copy = new ShoppingList()
            {
                Name = shoppingList.Name,
                Description = shoppingList.Description,
                Type = shoppingList.Type,
                Id = shoppingList.Id,
                IsOpened = shoppingList.IsOpened,
                Owner = shoppingList.Owner,
                ItemsCount = shoppingList.ItemsCount,
                DoneClickCommand = shoppingList.DoneClickCommand,
                EditClickCommand = shoppingList.EditClickCommand
            };

            return copy;
        }
    }
}