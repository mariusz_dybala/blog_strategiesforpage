using StrategiesForPage.Enums;

namespace StrategiesForPage.Models
{
    public class PageMessage
    {
        public ShoppingList ShoppingList { get; set; }
        public OperationType OperationType { get; set; }

        private PageMessage(ShoppingList shoppingList, OperationType operationType)
        {
            ShoppingList = shoppingList;
            OperationType = operationType;
        }

        public static PageMessage AsEdit(ShoppingList shoppingList)
        {
            var message = new PageMessage(shoppingList, OperationType.Edit);
            return message;
        }
        
        public static PageMessage AsAdd(ShoppingList shoppingList)
        {
            var message = new PageMessage(shoppingList, OperationType.Add);
            return message;
        }
    }
}