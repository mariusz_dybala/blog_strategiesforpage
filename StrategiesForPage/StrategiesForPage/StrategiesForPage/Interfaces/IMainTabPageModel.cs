using System;
using System.Threading.Tasks;
using StrategiesForPage.Models;
using StrategiesForPage.PageModels;
using StrategiesForPage.Pages;

namespace StrategiesForPage.Interfaces
{
    public interface IMainTabPageModel
    {
        void ItemClosed(ShoppingList shoppingList);
        event Action<ShoppingList> OnItemClosed;
        event Action<ShoppingList> OnItemAdded;
        event Action<ShoppingList> OnItemEdited;
    }
}