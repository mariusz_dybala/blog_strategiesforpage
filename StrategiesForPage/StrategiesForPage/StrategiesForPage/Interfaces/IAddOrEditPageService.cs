using System.Windows.Input;
using FreshMvvm;
using StrategiesForPage.Models;

namespace StrategiesForPage.Interfaces
{
    public interface IAddOrEditPageService
    {
        string ButtonText { get;}
        string HeaderText { get; }
        ShoppingList ShoppingListModel { get; }
        void OnButtonClicked(ShoppingList shoppingList, IPageModelCoreMethods coreMethods);
    }
}