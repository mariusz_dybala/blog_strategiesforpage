using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StrategiesForPage.Controls;
using StrategiesForPage.Interfaces;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace StrategiesForPage.Pages
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OpenedListsPage : TabPage
    {
        public override StackLayout SelectionIndicator => (StackLayout) GetTemplateChild("OpenedListsTabSelector");
        public OpenedListsPage()
        {
            InitializeComponent();
            
            TabIndex = ((TabButton) GetTemplateChild("OpenedListsTab")).TabButtonIndex;
        }
    }
}