using System;
using System.Threading.Tasks;
using System.Windows.Input;
using StrategiesForPage.Interfaces;
using StrategiesForPage.Models;
using FreshMvvm;
using PropertyChanged;
using StrategiesForPage.Enums;
using StrategiesForPage.Services;

namespace StrategiesForPage.PageModels
{
    [AddINotifyPropertyChangedInterface]
    public class MainTabPageModel : FreshBasePageModel, IMainTabPageModel
    {
        public event Action<ShoppingList> OnItemClosed;
        public event Action<ShoppingList> OnItemAdded;
        public event Action<ShoppingList> OnItemEdited;
        public ClosedListsPageModel ClosedListsPageModel { get; set; }
        public OpenedListsPageModel OpenedListsPageModel { get; set; }
        public ICommand CenterButtonClickedCommand => new FreshAwaitCommand(OnCenterButtonClicked);

        private int _activeTabIndex;

        public int ActiveTabIndex
        {
            get => _activeTabIndex;
            set => _activeTabIndex = value;
        }

        public MainTabPageModel()
        {
            ClosedListsPageModel = new ClosedListsPageModel(this);
            OpenedListsPageModel = new OpenedListsPageModel(this);
        }

        private async void OnCenterButtonClicked(object parameter, TaskCompletionSource<bool> taskCompletionSource)
        {
            await CoreMethods.PushPageModel<AddOrEditListPageModel>(new AddPageService(), true);
        }

        public override void ReverseInit(object returnedData)
        {
            if (returnedData is PageMessage pageMessage)
            {
                ItemAdded(pageMessage.ShoppingList);
            }
        }

        public void ItemClosed(ShoppingList shoppingList)
        {
            OnItemClosed?.Invoke(shoppingList);
        }

        private void ItemAdded(ShoppingList shoppingList)
        {
            OnItemAdded?.Invoke(shoppingList);
        }
    }
}