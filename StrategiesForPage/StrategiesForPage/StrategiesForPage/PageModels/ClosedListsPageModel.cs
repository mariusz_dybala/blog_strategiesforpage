using System.Collections.ObjectModel;
using FreshMvvm;
using StrategiesForPage.Interfaces;
using StrategiesForPage.Models;
using PropertyChanged;

namespace StrategiesForPage.PageModels
{
    [AddINotifyPropertyChangedInterface]
    public class ClosedListsPageModel : FreshBasePageModel
    {
        private readonly IMainTabPageModel _mainTabPageModel;

        public ObservableCollection<ShoppingList> ClosedLists { get; set; }

        public ClosedListsPageModel(IMainTabPageModel mainTabPageModel)
        {
            _mainTabPageModel = mainTabPageModel;
            _mainTabPageModel.OnItemClosed += OnItemClosed;

            ClosedLists = new ObservableCollection<ShoppingList>();
        }

        private void OnItemClosed(ShoppingList shoppingList)
        {
            ClosedLists.Add(shoppingList);
        }
    }
}