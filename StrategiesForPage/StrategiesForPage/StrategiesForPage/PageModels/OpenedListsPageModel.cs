using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using StrategiesForPage.Interfaces;
using StrategiesForPage.Models;
using PropertyChanged;
using Xamarin.Forms;
using FreshMvvm;
using StrategiesForPage.Services;

namespace StrategiesForPage.PageModels
{
    [AddINotifyPropertyChangedInterface]
    public class OpenedListsPageModel : FreshBasePageModel
    {
        private readonly IMainTabPageModel _mainTabPageModel;
        public ObservableCollection<ShoppingList> OpenedLists { get; set; }

        public OpenedListsPageModel(IMainTabPageModel mainTabPageModel)
        {
            _mainTabPageModel = mainTabPageModel;

            _mainTabPageModel.OnItemAdded += OnItemAdded;
            _mainTabPageModel.OnItemEdited += OnItemEdited;

            OpenedLists = new ObservableCollection<ShoppingList>
            {
                new ShoppingList
                {
                    Id = "0",
                    Name = "Saturday shopping",
                    Description = "We usually buy the food for the entire week",
                    IsOpened = true,
                    Owner = "Mariusz",
                    Type = "Meals",
                    ItemsCount = 10,
                    DoneClickCommand = new Command<string>(OnDoneList),
                    EditClickCommand = new Command<ShoppingList>(async (shoppingList) => await OnEditList(shoppingList))
                },
                new ShoppingList
                {
                    Id = "1",
                    Name = "Party time",
                    Description = "It's weekend! Party time :)",
                    IsOpened = true,
                    Owner = "Mariusz",
                    Type = "Drinks, Snacks",
                    ItemsCount = 8,
                    DoneClickCommand = new Command<string>(OnDoneList),
                    EditClickCommand = new Command<ShoppingList>(async (shoppingList) => await OnEditList(shoppingList))
                }
            };
        }

        private async Task OnEditList(ShoppingList shoppingList)
        {
            await CoreMethods.PushPageModel<AddOrEditListPageModel>(new EditPageService(shoppingList), true);
        }
        
        public override void ReverseInit(object returnedData)
        {
            if (returnedData is PageMessage pageMessage)
            {
                OnItemEdited(pageMessage.ShoppingList);
            }
        }

        private void OnItemEdited(ShoppingList shoppingList)
        {
            var oldItem = OpenedLists.FirstOrDefault(x => x.Id == shoppingList.Id);

            if (oldItem == null)
            {
                return;
            }

            oldItem.Description = shoppingList.Description;
            oldItem.Name = shoppingList.Name;
            oldItem.Owner = shoppingList.Owner;
            oldItem.Type = shoppingList.Type;

            OpenedLists = new ObservableCollection<ShoppingList>(OpenedLists.AsEnumerable());
        }

        private void OnItemAdded(ShoppingList shoppingList)
        {
            shoppingList.DoneClickCommand = new Command<string>(OnDoneList);
            shoppingList.DoneClickCommand = new Command<string>(OnDoneList);
            shoppingList.EditClickCommand =
                new Command<ShoppingList>(async (item) => await OnEditList(item));
            OpenedLists.Add(shoppingList);
        }

        private void OnDoneList(string listId)
        {
            var listToClose = OpenedLists.First(x => string.Equals(x.Id, listId));

            listToClose.IsOpened = false;

            OpenedLists.Remove(listToClose);
            _mainTabPageModel.ItemClosed(listToClose);
        }
    }
}