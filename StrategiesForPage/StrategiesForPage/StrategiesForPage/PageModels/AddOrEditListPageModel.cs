using System;
using System.Windows.Input;
using StrategiesForPage.Models;
using FreshMvvm;
using PropertyChanged;
using StrategiesForPage.Interfaces;
using Xamarin.Forms;

namespace StrategiesForPage.PageModels
{
    [AddINotifyPropertyChangedInterface]
    public class AddOrEditListPageModel : FreshBasePageModel
    {
        private  IAddOrEditPageService _addOrEditPageService;
        private ShoppingList _shoppingListModel;
        private string _buttonText;
        private string _headerText;

        public ShoppingList ShoppingListModel
        {
            get => _shoppingListModel;
            set => _shoppingListModel = value;
        }

        public string ButtonText
        {
            get => _buttonText;
            set => _buttonText = value;
        }
        
        public string HeaderText
        {
            get => _headerText;
            set => _headerText = value;
        }

        public ICommand ButtonClickedCommand => new Command(OnButtonClicked);
        public ICommand CancelClickedCommand => new Command(OnCancelClicked);

        public override void Init(object message)
        {
            if (message is IAddOrEditPageService addOrEditPageService)
            {
                _addOrEditPageService = addOrEditPageService;
                ShoppingListModel = ShoppingList.Copy(addOrEditPageService.ShoppingListModel);
                ButtonText = addOrEditPageService.ButtonText;
                HeaderText = addOrEditPageService.HeaderText;
            }
        }

        private void OnButtonClicked()
        {
            _addOrEditPageService.OnButtonClicked(ShoppingListModel, CoreMethods);
        }

        private void OnCancelClicked()
        {
            CoreMethods.PopPageModel(true, true);
        }
    }
}